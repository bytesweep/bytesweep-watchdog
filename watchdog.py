#!/usr/bin/env python3
import time
from model import get_complete_jobs, get_product_cves, put_watchdog_3p_cve, get_watchdog_3p_cve
import json


def product_version_compare(version1,version2):
	# version1 > version2 --> return 1
	# version1 < version2 --> return -1
	# version1 == version2 --> return 0
	ver1 = version1.split('.')
	ver2 = version2.split('.')
	n = len(ver1)
	if len(ver2) > n:
		n = len(ver2)
	for i in range(n):
		if i >= len(ver1):
			if ver2[i] == '0':
				i += 1
				continue
			else:
				return -1
		if i >= len(ver2):
			if ver1[i] == '0':
				i += 1
				continue
			else:
				return 1
		if ver1[i] > ver2[i]:
			return 1
		elif ver1[i] < ver2[i]:
			return -1
		i += 1
	return 0


def version_match_cve_rules(rules, version):
	rule_matches = []
	cve_match = False
	for rule in rules:
		compare = product_version_compare(version,rule['version_value'])
		if rule['version_affected'] == '=':
			if compare == 0:
				rule_matches.append(rule)
		elif rule['version_affected'] == '<':
			if compare < 0:
				rule_matches.append(rule)
		elif rule['version_affected'] == '<=':
			if compare <= 0:
				rule_matches.append(rule)
		elif rule['version_affected'] == '>':
			if compare > 0:
				rule_matches.append(rule)
		elif rule['version_affected'] == '>=':
			if compare >= 0:
				rule_matches.append(rule)
	if len(rule_matches) > 0:
		cve_match = True
	return {'cve_match':cve_match,'rule_matches':rule_matches}

def recheck_all_cves():
	jobs = get_complete_jobs()
	if not jobs:
		return
	for job in jobs:
		check_cves(job,True)

def check_cves(job, new):
	csv = '"Program","Version","CVE","Severity","Risk Score","NVD CVE Rule Match"'
	data_id = job['data_id']
	strings = job['data']['string_analysis']
	for string in strings:
		if string['type'] == 'program version' or string['type'] == 'library version':
			program = string['regex_name']
			version = string['match']
			match_id = string['match_id']
			vendor = string['vendor']
			prod_cves = get_product_cves(program)
			for prod_cve in prod_cves:
				cve_id = prod_cve['cve_id']
				prod_cve = prod_cve['data']
				last_modified_date_text = prod_cve['lastModifiedDate']
				cve_idnum = prod_cve['cve']['CVE_data_meta']['ID']
				#if cve_idnum != 'CVE-2019-0190':
				#	continue
				vendor_data = prod_cve['cve']['affects']['vendor']['vendor_data']
				for vendor_d in vendor_data:
					vendor_name = vendor_d['vendor_name']
					if vendor != '' and vendor != vendor_name:
						# wrong vendor: skip
						continue
					product_data = vendor_d['product']['product_data']
					for product_d in product_data:
						product_name = product_d['product_name']
						if product_name == program:
							version_data = product_d['version']['version_data']
							version_match_check = version_match_cve_rules(version_data, version)
							cve_match = version_match_check['cve_match']
							rule_matches = version_match_check['rule_matches']
							if cve_match:
								#print(program)
								#print(version)
								#print(cve_idnum)
								#if 'baseMetricV3' in prod_cve['impact']:
								#	print(prod_cve['impact']['baseMetricV3']['cvssV3']['baseSeverity'])
								#elif 'baseMetricV2' in prod_cve['impact']:
								#	print(prod_cve['impact']['baseMetricV2']['severity'])
								#print(rule_matches)
								new_cve = True
								watchdog_3p_cves = get_watchdog_3p_cve(data_id, cve_id, match_id)
								if len(watchdog_3p_cves) == 0:
									put_watchdog_3p_cve(data_id, cve_id, last_modified_date_text, new, rule_matches, match_id)
								# insert a rules match into db...
								
if __name__ == "__main__":
	while(True):
		recheck_all_cves()
		time.sleep(30)

