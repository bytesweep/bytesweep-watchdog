#!/usr/bin/env python
import psycopg2
import json
import bcrypt
import datetime
from ruamel.yaml import YAML
from psycopg2.extensions import AsIs

def dbconnect():
	config_file = open('/etc/bytesweep/config.yaml','r')
	yaml=YAML()
	config = yaml.load(config_file.read())
	config_file.close()
	try:
		conn = psycopg2.connect(host=config['dbhost'],dbname=config['dbname'], user=config['dbuser'], password=config['dbpass'])
		return conn
	except:
		print('ERROR: unable to connect to the database')
		return False

def dbclose(conn):
	conn.close()

def init_tables():
	c = init_watchdog_3p_cve_table()
	wm = init_watchdog_metadata_table()
	return c

def init_watchdog_3p_cve_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='watchdog_3p_cve')")
	if not cur.fetchone()[0]:
		cur.execute("create table watchdog_3p_cve (id serial NOT NULL primary key,data_id serial references data(data_id),cve_id serial references cve(cve_id),discover_date timestamp,last_modified_date_text text,new boolean NOT NULL DEFAULT FALSE, rule_matches jsonb,match_id int)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_watchdog_metadata_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='watchdog_metadata')")
	if not cur.fetchone()[0]:
		cur.execute("create table watchdog_metadata (id serial NOT NULL primary key,last_modified_date timestamp, last_modified_date_text text)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

#INSERT
def put_watchdog_3p_cve(data_id, cve_id, last_modified_date_text, new, rule_matches, match_id):
	conn = dbconnect()
	if not conn:
		return False
	if type(new) != type(True):
		return False
	cur = conn.cursor()
	discover_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:00")
	cur.execute("insert into watchdog_3p_cve (data_id,cve_id,discover_date,last_modified_date_text,new,rule_matches,match_id) values (%s,%s,%s,%s,%s,%s,%s)", (data_id,cve_id,discover_date,last_modified_date_text,AsIs(str(new)),json.dumps(rule_matches),match_id))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#SELECT
def get_watchdog_3p_cve(data_id, cve_id, match_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select cve.data,watchdog_3p_cve.data_id,watchdog_3p_cve.cve_id,watchdog_3p_cve.discover_date,watchdog_3p_cve.last_modified_date_text,watchdog_3p_cve.new,watchdog_3p_cve.rule_matches,watchdog_3p_cve.match_id from watchdog_3p_cve join cve on cve.cve_id=watchdog_3p_cve.cve_id where watchdog_3p_cve.data_id=%s AND watchdog_3p_cve.cve_id=%s AND watchdog_3p_cve.match_id=%s",(data_id,cve_id,match_id))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		watchdog_3p_cves = []
		for result in results:
			watchdog_3p_cves.append({'cve':result[0],'data_id':result[1],'cve_id':result[2],'discover_date':result[3],'last_modified_date_text':result[4],'new':result[5],'rule_matches':result[6],'match_id':result[7]})
		return watchdog_3p_cves
	else:
		return []

#SELECT
def get_data_id_cves(data_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select cve.data,watchdog_3p_cve.data_id,watchdog_3p_cve.cve_id,watchdog_3p_cve.discover_date,watchdog_3p_cve.last_modified_date_text,watchdog_3p_cve.new,watchdog_3p_cve.rule_matches,watchdog_3p_cve.match_id from watchdog_3p_cve join cve on cve.cve_id=watchdog_3p_cve.cve_id where watchdog_3p_cve.data_id=%s",(data_id,))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		watchdog_3p_cves = []
		for result in results:
			watchdog_3p_cves.append({'cve':result[0],'data_id':result[1],'cve_id':result[2],'discover_date':result[3],'last_modified_date_text':result[4],'new':result[5],'rule_matches':result[6],'match_id':result[7]})
		return watchdog_3p_cves
	else:
		return []

#SELECT
def get_latest_cve_metadata():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select last_modified_date_text from cve_metadata order by last_modified_date DESC limit 1;")
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return result[0]
	else:
		return False

#SELECT
def get_product_cves(product_name):
	conn = dbconnect()
	if not conn:
		return False
	# security check for cve_idnum
	if '"' in product_name or '\'' in product_name:
		print('SEC FAIL')
		return false
	cur = conn.cursor()
	cur.execute("select cve_id,data from cve where data @> '{\"cve\": {\"affects\":{\"vendor\":{\"vendor_data\":[{\"product\":{\"product_data\":[{\"product_name\":\"%s\"}]}}]}}}}'",(AsIs(product_name),))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		prod_cves = []
		for result in results:
			prod_cves.append({'cve_id':result[0],'data':result[1]})
		return prod_cves
	else:
		return []

#INSERT
def put_watchdog_metadata(last_modified_date_text):
	# convert date to postgres timestamp
	last_modified_date = last_modified_date_text
	if ":" == last_modified_date_text[-3:-2]:
		last_modified_date = last_modified_date[:-3]+last_modified_date[-2:]
	last_modified_date = datetime.datetime.strptime(last_modified_date, '%Y-%m-%dT%H:%M:%S%z')
	last_modified_date = last_modified_date.strftime("%Y-%m-%d %H:%M:00")
	conn = dbconnect()
	if not conn:
		return False
	# implement a check if the cve is already in the database
	#if get_job_by_name(job_name):
	#	return False
	cur = conn.cursor()
	cur.execute("insert into watchdog_metadata (last_modified_date,last_modified_date_text) values (%s,%s)", (last_modified_date,last_modified_date_text))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#SELECT
def get_cve(cve_idnum):
	conn = dbconnect()
	if not conn:
		return False
	# security check for cve_idnum
	if '"' in cve_idnum or '\'' in cve_idnum:
		return false
	cur = conn.cursor()
	cur.execute("select data from cve where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';",(AsIs(cve_idnum),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return result[0]
	else:
		return False

#SELECT
def get_latest_cve_metadata():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select last_modified_date_text from cve_metadata order by last_modified_date DESC limit 1;")
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return result[0]
	else:
		return False

#SELECT
def get_product_cves(product_name):
	conn = dbconnect()
	if not conn:
		return False
	# security check for cve_idnum
	if '"' in product_name or '\'' in product_name:
		print('SEC FAIL')
		return false
	cur = conn.cursor()
	cur.execute("select cve_id,data from cve where data @> '{\"cve\": {\"affects\":{\"vendor\":{\"vendor_data\":[{\"product\":{\"product_data\":[{\"product_name\":\"%s\"}]}}]}}}}'",(AsIs(product_name),))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		prod_cves = []
		for result in results:
			prod_cves.append({'cve_id':result[0],'data':result[1]})
		return prod_cves
	else:
		return []

#SELECT
def get_cve_count():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select count(data) from cve")
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return result[0]
	else:
		return False

#DELETE DATA
def delete_cve(cve_idnum):
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	# security check for cve_idnum
	if '"' in cve_idnum or '\'' in cve_idnum:
		return false
	cur = conn.cursor()
	cur.execute("delete from cve where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';",(AsIs(cve_idnum),))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#DROP
def drop_tables():
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("DROP TABLE watchdog_3p_cve")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#UPDATE
def update_cve(cve_idnum,cvedata):
	#TODO replace with valid check
	#if job_status not in valid_job_status:
	#	return False
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("UPDATE cve SET data=%s where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';", (json.dumps(cvedata), AsIs(cve_idnum)))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

#UPDATE
def bulk_update_cve(cvedata_list):
	#TODO replace with valid check
	#if job_status not in valid_job_status:
	#	return False
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	for cve_item in cvedata_list['CVE_Items']:
		cve_idnum = cve_item['cve']['CVE_data_meta']['ID']
		# security check for cve_idnum
		if '"' in cve_idnum or '\'' in cve_idnum:
			return false
		cur.execute("UPDATE cve SET data=%s where data @> '{\"cve\": {\"CVE_data_meta\":{\"ID\":\"%s\"}}}';", (json.dumps(cve_item), AsIs(cve_idnum)))
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def get_complete_jobs():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select jobs.job_name,jobs.job_status,jobs.job_project_basedir,jobs.job_filename,jobs.job_filepath,jobs.job_notes,data.data_id,data.data from data join jobs on data.job_id=jobs.job_id where jobs.job_status='done'")
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		jobs = []
		for result in results:
			jobs.append({'job_name':result[0],'job_status':result[1],'job_project_basedir':result[2],'job_filename':result[3],'job_filepath':result[4],'job_notes':result[5],'data_id':result[6],'data':result[7]})
		return jobs
	else:
		return False


